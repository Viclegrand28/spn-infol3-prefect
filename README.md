# INFO 328 PROJECT (IMPLEMENTATION OF SPN ALGORITHMS)

> ___ Note___ :  **Hello project members, please before do any modification, make sure that you're on you dev branch by _create_ or _switch_ on it**
>> - To create your branch:
>>> 1.  git checkout develop
>>> 2.  git checkout -b ma_branch
>> - Push personal modifications
>>> 1. git status 
>>> 2. git add (files) | all | .
>>> 3. git commit -m "hint of modification"
>>> if first push
>>> git push --set-upstream origin ma_branch
>>> else
>>> git push
>> - RollBack a commit
>>> 1. git log
>>> 2. locate the commit to revert and copy its hash
>>> 3. git revert -m 1 hash
>> - useful link
>>> [c libraries explanation](https://www.tutorialspoint.com/c_standard_library/c_function_gets.htm)

* Product Backlog:
  * [ ] 1. getting string method
  * [ ] 2. convert string to array(char) method
  * [ ] 3. convert char to binary method
  * [ ] 4. convert binary to int method
  * [ ] 5. convert int to binary method
  * [ ] 6. create global variables Pi_p (permutation matrix correspondance) Pi_s (substitution matrix correspondance)
  * [ ] 7. search on matrix method
  * [ ] 8. xor binary method
  * [ ] 9. substitution method
  * [ ] 10. permutation method
  * [ ] 11. convert binary to ascii
  * [ ] 12. extra GUI possibility

## Events Logs


> 
>>	|DateTime start|DateTime end|Edit Files|Action(s)|ToDo|App Status|Member|To do status|
>>	|--------------|------------|----------|---------|----|----------|----------|----------|
>>	|02-07-2021 9:41PM|03-07-2021 9:41PM|to complete|to complete|1|8%|Jessie|Pending|
>>	|02-07-2021 9:41PM|03-07-2021 9:41PM|to complete|to complete|2|16%|Jessie|Pending|
>>	|02-07-2021 9:41PM|03-07-2021 9:41PM|to complete|to complete|3|25%|Patrick|Pending|
>>	|02-07-2021 9:41PM|03-07-2021 9:41PM|to complete|to complete|4|33%|LOIC|Pending|
>>	|02-07-2021 9:41PM|03-07-2021 9:41PM|to complete|to complete|5|41%|Patrick|Pending|
>>	|02-07-2021 9:41PM|03-07-2021 9:41PM|to complete|to complete|6|49%|Jc|Pending|
>>	|02-07-2021 9:41PM|03-07-2021 9:41PM|to complete|to complete|7|57%|Jc|Pending|
>>	|02-07-2021 9:41PM|03-07-2021 9:41PM|to complete|to complete|8|65%|Messi|Pending|
>>	|03-07-2021 9:41PM|04-07-2021 9:41PM|to complete|to complete|9|73%|Messi|Pending|
>>	|03-07-2021 9:41PM|04-07-2021 9:41PM|to complete|to complete|10|81%|Victor|Pending|
>>	|02-07-2021 9:41PM|03-07-2021 9:41PM|to complete|to complete|11|90%|Loic|Pending|
>>	|03-07-2021 9:41PM|04-07-2021 9:41PM|to complete|to complete|12|100%|Victor|Pending|